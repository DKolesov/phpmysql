﻿<!DOCTYPE html>
<html>
<head>
	<title>Рабочее окно</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
	<div class="workwindow">
		<div class="head">
			<div style="float: left; color: #26292B; font-size: 25px; font-family: tahoma bold">АвтоИмпорт</div>
			<div style="float: right; margin-right: 10px;"><a href="index.php">Выйти</a> | <a href="index.php">Сменить пользователя</a></div>
		</div>
			<ul class="menu">
				<a href="zakaz.php"><li><span style="font-family: tahoma bold;">Оформить покупку</span></li></a>
				<a href="klient.php"><li>Клиенты</li></a>
				<a href="tovar.php"><li>Товары</li></a>
				<a href="tehn-harakter.php"><li>Технические характеристики</li></a>
			</ul>
		<div class="box">
		<form method="post" action="tehn-harakter.php">
			<h2>Добавить технические данные</h2>
			<!--Код товара: <input type="text" name="kodTovara"><br>-->
			Тип кузова: <input type="text" name="kuzov"><br>
			Количество дверей: <input type="text" name="dveri"><br>
			Количество мест: <input type="text" name="mesta"><br>
			Тип двигателя: <input type="text" name="dvigatel"><br>
			Расположение двигателя: <input type="text" name="raspDvigatel"><br>
			Раб. объем двигателя двигателя: <input type="text" name="objemDvigatel"><br>
			<input type="submit" name="b_add_har" value="Добавить технические характеристики" style="height: 30px;">
		</form>
		</div>	
		<div class="box">
		<form method="post" action="tehn-harakter.php">
			<h2>Добавить технические данные</h2>
			<!--Код товара: <input type="text" name="kodTovara2"><br>-->
			ID характеристики: <input type="text" name="id_har2"><br>
			Тип кузова: <input type="text" name="kuzov2"><br>
			Количество дверей: <input type="text" name="dveri2"><br>
			Количество мест: <input type="text" name="mesta2"><br>
			Тип двигателя: <input type="text" name="dvigatel2"><br>
			Расположение двигателя: <input type="text" name="raspDvigatel2"><br>
			Раб. объем двигателя двигателя: <input type="text" name="objemDvigatel2"><br>
			<input type="submit" name="b_change_har" value="Изменить технические характеристики" style="height: 30px;">
		</form>

		<?php 
			require_once("linkdb.php");

			if(isset($_POST['b_add_har'])) {
				#$kodTovara = strip_tags(trim($_POST['kodTovara']));
				$kuzov = strip_tags(trim($_POST['kuzov']));
				$dveri = strip_tags(trim($_POST['dveri']));
				$mesta = strip_tags(trim($_POST['mesta']));
				$dvigatel = strip_tags(trim($_POST['dvigatel']));
				$raspDvigatel = strip_tags(trim($_POST['raspDvigatel']));
				$objemDvigatel = strip_tags(trim($_POST['objemDvigatel']));

				mysqli_query($link, " INSERT INTO teh_dan (tip_kuzova, kol_dverei, kol_mest, tip_dvigatel, raspolozh_dvigatel, obj_dvigatel)
									  VALUES ('$kuzov', '$dveri', '$mesta', '$dvigatel', '$raspDvigatel', '$objemDvigatel')
					");

				mysqli_close($link);
				echo "Характеристика добавлена.";
			}

			if(isset($_POST['b_change_har'])) {
				#$kodTovara = strip_tags(trim($_POST['kodTovara']));
				$id_har2 = strip_tags(trim($_POST['id_har2']));
				$kuzov2 = strip_tags(trim($_POST['kuzov2']));
				$dveri2 = strip_tags(trim($_POST['dveri2']));
				$mesta2 = strip_tags(trim($_POST['mesta2']));
				$dvigatel2 = strip_tags(trim($_POST['dvigatel2']));
				$raspDvigatel2 = strip_tags(trim($_POST['raspDvigatel2']));
				$objemDvigatel2 = strip_tags(trim($_POST['objemDvigatel2']));

				mysqli_query($link, " UPDATE teh_dan
									  SET tip_kuzova = '$kuzov2', kol_dverei = '$dveri2', kol_mest = '$mesta2', tip_dvigatel = '$dvigatel2',
									  raspolozh_dvigatel = '$raspDvigatel2', obj_dvigatel = '$objemDvigatel2'
									  WHERE $id_har2 = id
					");

				mysqli_close($link);
				echo "Характеристика изменена.";
			}
		?>

		<table bgcolor="#fff" width="980px" cellpadding="5" frame="below">
			<caption>Характеристики</caption>
				<tr>
					<th width="20px" align="left">ID характеристики</th>
					<th width="150px" align="left">Тип кузова</th>
					<th width="100px" align="left">Количество дверей</th>
					<th width="150px" align="left">Количество мест</th>
					<th width="130px" align="left">Тип двигателя</th>
					<th width="" align="left">Расположение двигателя</th>
					<th width="50" align="left">V - двигателя</th>
				</tr>
		</table>		
			<?php

				$result = mysqli_query($link, " SELECT * FROM teh_dan ");
				while ($row = mysqli_fetch_array($result)) {
			?>
			<table bgcolor="#fff" width="980px" cellpadding="5" frame="below">
				<tr>
					<td width="20px" align="left"><?php echo $row['id'] ?></td>
					<td width="150px" align="left"><?php echo $row['tip_kuzova'] ?></td>
					<td width="100px" align="left"><?php echo $row['kol_dverei'] ?></td>
					<td width="150px" align="left"><?php echo $row['kol_mest'] ?></td>
					<td width="130px" align="left"><?php echo $row['tip_dvigatel'] ?></td>
					<td width="" align="left"><?php echo $row['raspolozh_dvigatel'] ?></td>
					<td width="50" align="left"><?php echo $row['obj_dvigatel'] ?></td>
				</tr>
			</table>
			<?php
				}
			?>

		</div>		
	</div>
</body>
</html>